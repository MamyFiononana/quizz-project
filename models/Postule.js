const Sequelize = require('sequelize')
const db = require("../database/db.js")
const { timeStamp } = require('console')

 
module.exports= db.sequelize.define(
    'postule',
    {
        id: {
            type: Sequelize.INTEGER,

            references: 
            {
                model: 'User',
                key: 'id',
            }
        },
        id_offre:{
            type:Sequelize.INTEGER,
            references: 
            {
                model: 'Offre',
                key: 'id_offre',
            }
        },
        datePost:{
            type:Sequelize.DATE,
        },
       
        createdAt:{
            type:Sequelize.DATE,
            defaultValue: Sequelize.NOW
        },
        updatedAt:{
            type:Sequelize.DATE,
            defaultValue: Sequelize.NOW
        }
    },
        {
            timeStamp:false
        }
    )
