const express = require("express")
const offres = express.Router()
const cors = require("cors")
const jwt = require("jsonwebtoken")
const bcrypt = require("bcrypt")

const Offre = require("../models/Offre")
offres.use(cors())

process.env.SECRET_KEY = 'secret'

offres.post('/recruteur', (req, res)=>{
    const today = new Date()
    const offreData = {
        id_offre: req.body.id_offre,
        nom : req.body.nom,
        addre : req.body.addre,	
        titre : req.body.titre,
        activite : req.body.activite,
        missions : req.body.missions,	
        profile : req.body.profile,
        reference : req.body.reference,
        dateLimiteOffre:req.body.dateLimiteOffre,
        datePub : req.body.datePub,
        limiteOffre :req.body.limiteOffre,
        id:req.body.id,
        createdAt : today,	
        updatedAt : today
    }
    Offre.findOne({
        where:{
           nom: req.body.nom,
        },
    })
    .then(offre=>{
         console.log(offre)
        if (!offre){
            Offre.create(offreData)
            .then(offre =>{
                let token = jwt.sign(offre.dataValues, process.env.SECRET_KEY,{
                    expiresIn: 1440
                });
                console.log('success', offre.toJSON());
                res.json({token:token})
            })
            .catch(err=>{
                console.log(err, req.body.nom);
                res.send('error:'+err)
            })
            }
            else{
                res.json("Offre existe déjà")
        }
        
    })
    .catch(err=>{
        res.send('error:'+err)
    })
})

//Afficher offre
offres.get('/offre', (req, res)=>{
    Offre.findAll()
    .then(offre=>{
        if(offre){
            res.json(offre)
        }else{
            res.send("Offre n'existe pas")
        }
    })
    .catch(err=>{
        res.send('error:' + err)
    })
})


module.exports = offres