const express = require("express")
const users = express.Router()
const cors = require("cors")
const jwt = require("jsonwebtoken")
const bcrypt = require("bcrypt")
const nodemailer = require('nodemailer');//importing node mailer
const xoauth2 = require('xoauth2');
const User = require("../models/User")
users.use(cors())

process.env.SECRET_KEY = 'secret'

//INSCRIPTION
users.post('/inscription', (req, res)=>{
    const today = new Date()
    const userData = {
        firstName : req.body.firstName,
        lastName : req.body.lastName,	
        username : req.body.username,
        email : req.body.email,
        password : req.body.password,	
        id_role: req.body.id_role,
        createdAt : today,	
        updatedAt : today
    }
    User.findOne({
        where: {
            email: req.body.email,
        },
    })
    .then(user=>{
        if (!user){
            const hash = bcrypt.hashSync(userData.password, 10)
            userData.password = hash
            User.create(userData)
            .then(user =>{
                let token = jwt.sign(user.dataValues, process.env.SECRET_KEY,{
                    expiresIn: 1440
                });
                var mailer = require("nodemailer");
                var xoauth2 = require('xoauth2');

                // Use Smtp Protocol to send Email
                var smtpTransport = mailer.createTransport({
                    service: "gmail",
                    auth: {xoauth2: xoauth2.createXOAuth2Generator({
                        type: "OAuth2",
                        user: "mamayfiononana@gmail.com",
                        clientId: '437807372971-eavr9mp96bgabl62t9qok8c2n3tr3qo6.apps.googleusercontent.com' ,
                        clientSecret: 'fOGjqatWeEZwwdBaDDYKOnGq'
                    })
                    }
                });
                
                var mail = {
                    from: "<mamyfiononana@gmail.com>",
                    to: "mamyfiononana@gmail.com",
                    subject: "Send Email Using Node.js",
                    text: "Node.js New world for me",
                    html: "<b>Node.js New world for me</b>"
                }
                
                smtpTransport.sendMail(mail, function(error, response){
                    if(error){
                        console.log(error);
                    }else{
                        console.log("Message sent: " + response.message);
                    }
                
                    smtpTransport.close();
                });


                res.json({token:token})
            })
            .catch(err=>{
                res.send('error:'+err)
            })
            }
            else{
                res.json({error:'Utilisateur existe  déjà '})
        }
        
    })
    .catch(err=>{
        res.send('error:'+err)
    })
})

//ATHENTIFICATION
users.post('/authentification', (req, res)=>{
    User.findOne({
        where:{
            email: req.body.email}
        })
        .then(user=>{
            if(bcrypt.compareSync(req.body.password, user.password)){
                let token = jwt.sign(user.dataValues, process.env.SECRET_KEY,{
                    expiresIn:1440
                })
                res.json({token:token})
            }else{
                res.send("Utilisateur n'existe pas" )
            }
        })
        .catch(err=>{
            res.send('error:' + err)
        })
    })
    //PROFILE
    users.get('/liste', (req, res)=>{
        // var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

        User.findAll()
        .then(user=>{
            if(user){
                res.json(user)
            }else{
                res.send("Utilisateur n'existe pas")
            }
        })
        .catch(err=>{
            res.send('error:' + err)
        })
    })

module.exports = users