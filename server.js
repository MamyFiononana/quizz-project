var express = require("express")
var cors = require ("cors")
var bodyParser = require("body-parser")
var app = express()
var port = process.env.PORT || 4300

app.use(bodyParser.json())
app.use(cors())
app.use(
    bodyParser.urlencoded({extended:false})
)

var Users = require("./routes/Users")
var Offres = require("./routes/Offres")
var Roles = require("./routes/Roles")
const db = require("./database/db")


app.use("/users", Users)
app.use("/offres", Offres)
app.use("/roles", Roles)


app.listen(port, function(){
    console.log("Server is running on port" + port)
})